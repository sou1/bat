@powershell -WindowStyle Hidden -NoProfile -ExecutionPolicy Unrestricted "$s=[scriptblock]::create((gc \"%~f0\"|?{$_.readcount -gt 1})-join\"`n\");&$s" %*&goto:eof

$assemblies = (
	"System",
	"PresentationCore",
	"PresentationFramework",
	"WindowsBase",
	"System.Xml",
	"System.Xaml"
)

$xaml = @"
<Window
  xmlns = "http://schemas.microsoft.com/winfx/2006/xaml/presentation"
  xmlns:x = "http://schemas.microsoft.com/winfx/2006/xaml"
  Title = "digits"
  ResizeMode = "NoResize"
  WindowStyle = "None"
  AllowsTransparency = "true"
  Background = "transparent"
  Topmost = "true"
  Width = "Auto"
  Height = "Auto"
  Top = "0"
  Left = "-800"
>
<StackPanel>
  <TextBlock Name="tb" FontFamily="Old English Text MT" FontSize="96px" FontWeight="Light" Foreground="White" Width="Auto" Height="Auto">
    <!-- FontWeight -> Thin ExtraLight Light Normal Medium SemiBold Bold ExtraBold Black ExtraBlack -->
    <!-- Watch Format... (HH:mm)(MM/dd HH:mm:ss)etc -->
    HH:mm
    <TextBlock.Effect>
      <DropShadowEffect>
        <DropShadowEffect.ShadowDepth>0</DropShadowEffect.ShadowDepth>
        <DropShadowEffect.BlurRadius>6</DropShadowEffect.BlurRadius>
      </DropShadowEffect>
    </TextBlock.Effect>
  </TextBlock>
</StackPanel>
</Window>
"@


$src = @"

using System;
using System.IO;
using System.Xml;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Threading;


	public class digits{

		private static Window w;
		private static TextBlock tb;
		private static DispatcherTimer dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal);
		private static string tbt;
		private static System.Diagnostics.PerformanceCounter pc = new System.Diagnostics.PerformanceCounter();


		[STAThread]
		public static void Main(string[] args){

			Application app = new Application();

			w = null;
			w = (Window)XamlReader.Parse(args[0]);
			if (w.FindName("tb") != null) {
				tb = (TextBlock)w.FindName("tb");
				tbt = tb.Text;
			}

			w.MouseLeftButtonDown += delegate{w.DragMove();};
			w.MouseRightButtonUp += delegate{w.Close();};

			pc.CategoryName = "memory";
			pc.CounterName = "Available MBytes";
			pc.MachineName = ".";


			dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
			dispatcherTimer.Interval = new TimeSpan(0,0,1);
			dispatcherTimer.Start();

			dispatcherTimer_Tick(null,null);

			app.Run( w );

		}

		static void dispatcherTimer_Tick(object sender, EventArgs e)
		{
			tb.Text = DateTime.Now.ToString(tbt);
			w.Width = tb.ActualWidth;
			w.Height = tb.ActualHeight;
			w.Left = 1900 - w.Width;
		}
	}
"@

Add-Type -ReferencedAssemblies $assemblies -TypeDefinition $src -Language CSharp
[digits]::main($xaml)
